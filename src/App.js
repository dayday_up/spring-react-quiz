import React from 'react';
import logo from './logo.svg';
import './App.css';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { init } from './actions/app';
import { get } from './common/js/request'

class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      name: "app"
    }
  }

  componentDidMount() {
    get('/api/test1')
      .then((response) => {
        console.log(response);
      })
  }

  handleClick = () => {
    this.setState({
      name: "test"
    })
  };

  render() {
    const name = this.props.app.name;
    return (
      <div>
        <p>{name}</p>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  app: state.app
});

const mapDispatchToProps = dispatch => bindActionCreators({
  init
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
